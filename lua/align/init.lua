local M = {}

M.setup = function(user_config)
	user_config = user_config or {}
	-- Only using tbl_extend instead of tbl_deep_extend so config.opts couldn't be override
	M.config = vim.tbl_extend('force', M.default_config, user_config)

	_G.AlignConfig = vim.tbl_extend('force', M.config, {})

	-- Main commands
	vim.api.nvim_create_user_command('Align', M.align, M.opts)
	vim.api.nvim_create_user_command('AlignVisual', M.align_visual, M.opts)
	-- Util commands
	vim.api.nvim_create_user_command('AlignChangeMode', M.change_alignement, { nargs = 1, bar = true })
	vim.api.nvim_create_user_command('AlignChangeMode', M.change_alignement, { nargs = 1, bar = true })
	vim.api.nvim_create_user_command('AlignChangeLength', M.change_minimal_Length, { nargs = 1, bar = true })
	vim.api.nvim_create_user_command('AlignReset', M.reset_config, { nargs = '*' })
end

M.align = function(cmd)
	local config = M.__copy(_G.AlignConfig)
	local start_line, text, bang = cmd.line1 - 1, cmd.args, cmd.bang

	local lines, length = M._generate_formated_text(config, start_line, { text })
	M._write_lines_to_buffer(config, start_line, lines, length, bang)
end

M.align_visual = function(cmd)
	local config = M.__copy(_G.AlignConfig, { override = true })
	-- Always overriding when using AlignVisual
	-- Remove one from the starting line because
	-- get_lines/set_lines are 0-based indexing.
	local start_line, end_line = cmd.line1 - 1, cmd.line2
	local bang = cmd.bang

	local buffer_lines = vim.api.nvim_buf_get_lines(0, start_line, end_line, true)

	local lines, length = M._generate_formated_text(config, start_line, buffer_lines)
	M._write_lines_to_buffer(config, start_line, lines, length, bang)
end

M.change_alignement = function(cmd)
	_G.AlignConfig.alignement = cmd.args
end

M.change_minimal_Length = function(cmd)
	_G.AlignConfig.minimal_length = tonumber(cmd.args, 10) or M.config.minimal_length
end

M.reset_config = function(cmd)
	local keys = cmd.fargs

	if #keys == 0 then
		_G.AlignConfig = M.__copy(M.config)
		return
	end

	for _, key in ipairs(keys) do
		_G.AlignConfig[key] = M.config[key]
	end
end

-- helpers
M._generate_formated_text = function(config, start_line, texts)
	local length = M._work_on_lines(config, texts)

	local parts_total_length
	for i, value in ipairs(texts) do
		parts_total_length = length - #value
		texts[i] = M._line(start_line + i, value, parts_total_length)
	end

	return texts, length
end

M._write_lines_to_buffer = function(config, start, lines, length, surrounded)
	local fn = M._insert
	if config.override then
		fn = M._override
	end

	for i, line in ipairs(lines) do
		fn(start, line, i - 1)
	end

	if surrounded then
		M._surround(config, start, #lines, length)
	end
end

M._insert = function(start, line)
	vim.api.nvim_buf_set_lines(0, start, start, true, { line })
end

M._override = function(start, line, offset)
	local position = start + offset

	-- If the pcall fail, it must be because an overriding was asked while
	-- being on the last line of the file. We cannot write on non-existing line
	-- instead we append it.
	local ok = pcall(vim.api.nvim_buf_set_lines, 0, position, position + 1, true, { line })
	if not ok then
		M._insert(start, line)
	end
end

M._work_on_lines = function(config, texts)
	local length = config.minimal_length

	local text
	for i = 1, #texts do
		text = texts[i]

		if config.remove_indent then
			text = string.gsub(text, '^%s+', '')
		else
			-- We replace tabulations with spaces so #text will
			-- return the real number of characters.
			text = M.__replace_tab(text)
		end

		if #text > length then
			length = #text
		end

		texts[i] = text
	end

	return length
end

M._line = function(number, text, length)
	-- Retrieving 2 from the parts' length because we need to take
	-- into account the spaces, which will be added in M._content
	local left, right = M._parts(length)
	local content = M._content(text)
	local unindented = M._comment(left .. content .. right)
	local line = M._indent(number, unindented)
	return line
end

M._comment = function(text)
	local comment_str = vim.api.nvim_buf_get_option(0, 'commentstring')

	return string.format(comment_str, text)
end

M._content = function(text)
	return ' ' .. text .. ' '
end

M._parts = function(length)
	local config = _G.AlignConfig
	return M.__tbl_parts[config.alignement](config, length)
end

M._indent = function(linenumber, text)
	local config = _G.AlignConfig
	-- vim.fn.indent is not 0-based
	local indent = vim.fn.indent(linenumber)
	local indent_character = ' '
	if indent == 0 then
		return text
	end

	local line = vim.api.nvim_buf_get_lines(0, linenumber - 1, linenumber, true)[1]
	local indent_type = string.find(line, '^\t+')
	if indent_type then
		indent = indent / vim.fn.shiftwidth()
		indent_character = '\t'
	end

	return string.rep(indent_character, indent) .. text
end

M._surround = function(config, start, nb_lines, length)
	-- Adding one allow us to target the line RIGHT AFTER
	-- the last inserted line
	local end_line = start + nb_lines + 1
	-- By using start + 1 we use the indent of the first
	-- inserted line instead of the line where our cursor was
	-- when we called the command.
	local empty = M._empty(config, start + 1, length)

	M._insert(start, empty)
	M._insert(end_line, empty)
end

M._empty = function(config, start, length)
	config.fill_up = config.surrounding_fill_up
	local left, right = M.__tbl_parts[config.alignement](config, length + 2)
	local comment = M._comment(left .. right)

	return M._indent(start, comment)
end

-- variables
M.default_config = {
	-- default align modifier
	align = 'left',
	-- default border size
	-- this will be doubled and added to the length maximal text
	-- length found by _maximum_length.
	border = 10,
	-- define if the interior of our parts should be filled
	-- using config.filler or spaces.
	fill_up = false,
	-- default filler character
	filler = '=',
	-- minimal length for align items
	-- this will be supressed by any longer text passed to
	-- the command.
	minimal_length = 30,
	-- should white characters at the beginning of line be removed
	remove_indent = false,
	-- when using ! option, should those lines be filled using config.filler or <space>
	surrounding_fill_up = true,
	-- should Align command override the current line or be inserted
	override = false,
	-- should spaces be used or tabulation for generated lines' indent
	use_spaces = false,
}

M.opts = {
	nargs = '*',
	bang = true,
	range = true,
}

-- utils
M.__tbl_parts = setmetatable({
	['center'] = function(config, length)
		local border, rest = M.__tbl_parts.border_and_text(config, length / 2)
		local fill_on_odd = length % 2 == 0 and '' or config.filler

		return border .. rest, rest .. border .. fill_on_odd
	end,
	['left'] = function(config, length)
		local border, rest = M.__tbl_parts.border_and_text(config, length)

		return border, rest .. border
	end,
	['right'] = function(config, length)
		local border, rest = M.__tbl_parts.border_and_text(config, length)

		return border .. rest, border
	end,
}, {
	__index = function()
		return M.__tbl_parts['left']
	end
})

M.__tbl_parts.border_and_text = function(config, length)
	local border = string.rep(config.filler, config.border)
	local to_fill_with = config.fill_up and config.filler or ' '
	local rest = string.rep(to_fill_with, length)

	return border, rest
end

M.__copy = function(tbl, override)
	override = override or {}
	local new_tbl = {}

	local value
	for k, v in pairs(tbl) do
		value = override[k] or v
		new_tbl[k] = value
	end

	return new_tbl
end

M.__replace_tab = function(text)
	local sub_tab = string.rep(' ', vim.fn.shiftwidth())
	local _, nb_tab = string.find(text, '^\t+')
	if not nb_tab then
		return text
	end

	local replacement = string.rep(sub_tab, nb_tab)
	return string.gsub(text, '^\t+', replacement)
end

return M
